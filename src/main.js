// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Router from './routes.js'
import Header from '@/components/includes/Header'
import Navbar from '@/components/includes/Navbar'
import axios from 'axios'
window.axios = axios.create({
	baseURL:`http://thewificonnect.com/`
});

Vue.config.productionTip = false
Vue.component('app-header',Header)
Vue.component('app-navbar',Navbar)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router:Router,
  components: { App },
  template: '<App/>'
})
