import VueRouter from 'vue-router'
import Vue from 'vue'
import Login from '@/components/Login'
import Dashboard from '@/components/Dashboard'
import User from '@/components/includes/User'

Vue.use(VueRouter)
const router = new VueRouter({
	routes:[
	{	path:'/login', component:Login	},
	{	path:'/', component:Dashboard 	},
	{	path:'/user', component:User 	},
	]
})
export default router
